import socketserver

class HandlerTcpServer(socketserver.BaseRequestHandler):

    def handle(self):
        self.data = self.request.recv(1024).strip()
        print("from {} got:".format(self.client_address[0]))
        print(self.data)
        self.request.sendall("ACK from TCP Server".encode())

if __name__ == "__main__":
    host, port = "127.0.0.16", 1025 
    tcp_server = socketserver.TCPServer((host, port), HandlerTcpServer)
    tcp_server.serve_forever()
