#ifndef MAILDIR_H
#define MAILDIR_H

#include <dirent.h>

DIR *open_new_dir();

void get_new_mail_path(char *file, char *result);

#endif // MAILDIR_H

