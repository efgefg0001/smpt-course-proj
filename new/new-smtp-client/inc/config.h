#ifndef CONFIG_H
#define CONFIG_H

#define WORKERS_COUNT 4
#define MAIL_DIR "/home/alex/maildir"
#define MAX_FILENAME_LEN 255
#define DNS_SERVER "127.0.0.1"
#define CLIENT_NAME "smtp_client"
#define LOG_PATH "/tmp/smtp-client.log"


#endif // CONFIG_H

