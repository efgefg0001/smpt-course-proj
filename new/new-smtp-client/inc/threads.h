#ifndef THREADS_H
#define THREADS_H

#include "mail_q.h"

void set_zmq_ctx(void* ctx);

void start_workers();
void wait_workers();

int read_mail_from_maildir(char *filename, struct Mail *mails, int *emails_count);


#endif // THREADS_H

