#ifndef DNS_H
#define DNS_H

#include "mail_q.h"

int resolve_mx_ip(struct Mail *mail, char *got_ip);

#endif // DNS_H

