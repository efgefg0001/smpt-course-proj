#ifndef MAIL_Q_H
#define MAIL_Q_H

#define MAIL_PATH_MAX_SIZE 255

struct MailQ
{
    void* zmq_socket;
    int is_writer;
};

#define MAX_MAIL_DATA_LEN 1000
#define MAX_ADDR_LEN 100


struct Mail
{
    char email_addrs[20][MAX_ADDR_LEN];
    int email_addrs_count;
    char status[MAX_ADDR_LEN];
    char data[MAX_MAIL_DATA_LEN];
    char domain[MAX_ADDR_LEN];
    char from[MAX_ADDR_LEN];

};

struct MailQ new_MailQ(void* zmq_context, int key, int is_writer);
void close_MailQ(struct MailQ mail_q);
int add_to_MailQ(struct MailQ *mail_q, char *mail_path);
//int32_t message_queue_pop(MessageQueue const* mq, Message** message);
int get_from_MailQ_blocking(struct MailQ *mail_q, char *mail_path);


#endif // MAIL_Q_H

