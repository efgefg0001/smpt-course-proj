#ifndef REMORES_H
#define REMORES_H

#include "poll.h"
#include "mail_q.h"


int process_got_mail(char *mail_path);

int connect_to_ip(char *ip, int *descriptor);


struct Domains {
    struct pollfd pollfds[20];
    struct Mail mails[20];
    char resolved_ips[20][50];

    int count;
};

int fill_domains(struct Mail *mails, int mails_count, struct Domains *domains);
int send_mail_for_domain(struct Domains *domains, int i);

void free_resources(struct Domains *domains);

#define EXPECTED_EVENTS POLLIN | POLLOUT

#endif // REMORES_H

