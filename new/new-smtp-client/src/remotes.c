#include "remotes.h"

#include "errno.h"
#include <poll.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "threads.h"
#include "dns.h"
#include "config.h"
#include <string.h>

#include <stdio.h>
#include "log.h"


int process_got_mail(char *mail_path)
{
    struct Mail mails[20];
    int count = 0;
    read_mail_from_maildir(mail_path, mails, &count);

    struct Domains domains;
    fill_domains(mails, count, &domains);

    poll(domains.pollfds, domains.count, 500);

    for (int i = 0; i < domains.count; ++i) {
        if (domains.pollfds[i].revents & EXPECTED_EVENTS) {
            int ret = send_mail_for_domain(&domains, i);
            if (ret != 0)
               strcpy(domains.mails[i].status, "FAILED");
        } else {
            printf("error");
        }
    }

    free_resources(&domains);

    return 0;
}

int connect_to_ip(char *ip, int *descriptor)
{
    int socket_desr = socket(AF_INET, SOCK_STREAM, 0);//| SOCK_NONBLOCK, 0);
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(1025);
    inet_pton(AF_INET, ip, &serv_addr.sin_addr);
    int cr = connect(socket_desr, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
    if (cr == 0 || (cr == -1 && errno == EINPROGRESS)) {
        *descriptor = socket_desr;
        return 0;
    } else {
        return -100;
    }
}

int fill_domains(struct Mail *mails, int mails_count, struct Domains *domains)
{
    domains->count = 0;
    for (int i = 0; i < mails_count; ++i) {
        struct Mail curMail = mails[i];
        char resolved_ip[50];
        int result = resolve_mx_ip(&curMail, resolved_ip);
        if (result == 0) {
            int descr;
            result = connect_to_ip(resolved_ip, &descr);
            if (result == 0) {
                strcpy(curMail.status, "STARTED");
                domains->mails[domains->count] = curMail;
                strcpy(domains->resolved_ips[domains->count], resolved_ip);
                struct pollfd pfd;
                pfd.fd=descr;
                pfd.events = EXPECTED_EVENTS;
                domains->pollfds[domains->count] = pfd;
                domains->count += 1;
            } else {
                fprintf(stderr, "could not connect to ip: %s\n", resolved_ip);
            }
        } else {
            fprintf(stderr, "error in dns resolution %s\n", curMail.domain);
        }
    }
    return 0;
}

int send_mail_for_domain(struct Domains *domains, int i)
{
    int ret = connect_command(domains, i);
    if (ret != 0) {
        return -1;
    }

    ret = helo_command(domains, i);
    if (ret != 0) {
        return -1;
    }

    ret = mail_from_command(domains, i);
    if (ret != 0) {
        return -1;
    }

    ret = rcpt_to_command(domains, i);
    if (ret != 0) {
        return -1;
    }

    ret = data_command(domains, i);
    if (ret != 0) {
        return -1;
    }

    ret = quit_command(domains, i);
    if (ret != 0) {
        return -1;
    }



    return 0;
}



int connect_command(struct Domains *domains, int i) {
    int socket_descr = domains->pollfds[i].fd;
    const int size = 100;
    char buffer[size];
    int read_bytes = read(socket_descr, buffer, size);
    int code;
    sscanf(buffer, "%d", &code);
    if (code != 220) {
        return -1;
    }

    return 0;
}

int helo_command(struct Domains *domains, int i) {
    int socket_descr = domains->pollfds[i].fd;
    const int size = 100;
    char request[size];
    sprintf(request, "HELO %s\r\n", CLIENT_NAME);
    int written = write(socket_descr, request, strlen(request));

    to_log("Sent command: %s", request);

    char response[size];
    int read_bytes = read(socket_descr, response, size);

    to_log("Got response: %s", response);


    int code;
    sscanf(response, "%d", &code);
    if (code != 250) {
        return -1;
    }

    return 0;
}



int mail_from_command(struct Domains *domains, int i) {
    int socket_descr = domains->pollfds[i].fd;
    const int size = 100;
    char request[size];
    sprintf(request, "MAIL FROM: %s\r\n", domains->mails[i].from);
    int written = write(socket_descr, request, strlen(request));

    to_log("Sent command: %s", request);

    char response[size];
    int read_bytes = read(socket_descr, response, size);

    to_log("Got response: %s", response);

    int code;
    sscanf(response, "%d", &code);
    if (code != 250) {
        return -1;
    }

    return 0;
}

int rcpt_to_command(struct Domains *domains, int i) {
    int socket_descr = domains->pollfds[i].fd;
    struct Mail mail = domains->mails[i];
    for (int i = 0; i < mail.email_addrs_count; ++i) {
        const int size = 100;
        char request[size];
        sprintf(request, "RCPT TO: %s\r\n", mail.email_addrs[i]);
        int written = write(socket_descr, request, strlen(request));

        to_log("Sent command: %s", request);

        char response[size];
        int read_bytes = read(socket_descr, response, size);

        to_log("Got response: %s", response);

        int code;
        sscanf(response, "%d", &code);
        if (code != 250) {
            return -1;
        }
    }
    return 0;
}

int data_command(struct Domains *domains, int i) {
    int socket_descr = domains->pollfds[i].fd;
    struct Mail mail = domains->mails[i];
    const int size = 500;
    char request[size];
    sprintf(request, "DATA \r\n");
    int written = write(socket_descr, request, strlen(request));
    to_log("Sent command: %s", request);

    char response[size];
    int read_bytes = read(socket_descr, response, size);

    to_log("Got response: %s", response);

    int code;
    sscanf(response, "%d", &code);
    if 	(code != 354) {
        return -1;
    }


    char *pch = strtok (mail.data,"\n");
    while (pch != NULL)
    {
        strcpy(request, pch);
        strcat(request, "\r\n");
        write(socket_descr, request, strlen(request));

        to_log("Sent string: %s", request);

        pch = strtok (NULL, "\n");
    }


    sprintf(request, ".\r\n");
    written = write(socket_descr, request, strlen(request));
    to_log("Sent string: %s", request);

    response[size];
    read_bytes = read(socket_descr, response, size);
    to_log("Got response: %s", response);

    code;
    sscanf(response, "%d", &code);
    if 	(code != 250) {
        return -1;
    }


    return 0;
}

int quit_command(struct Domains *domains, int i) {
    int socket_descr = domains->pollfds[i].fd;
    struct Mail mail = domains->mails[i];
    const int size = 100;
    char request[size];
    sprintf(request, "QUIT\r\n");
    int written = write(socket_descr, request, strlen(request));

    to_log("Sent command: %s", request);


    char response[size];
    int read_bytes = read(socket_descr, response, size);

    to_log("Got response: %s", response);


    int code;
    sscanf(response, "%d", &code);
    if 	(code != 221) {
        return -1;
    }
    return 0;
}

void free_resources(struct Domains *domains)
{
   for (int i = 0; i < domains->count; ++i) {
       close(domains->pollfds[i].fd);
   }
}
