#include "maildir.h"

#include <stdio.h>
#include <stdlib.h>
#include <uuid/uuid.h>

#include "config.h"

DIR *open_new_dir() {
    char *maildir_root = MAIL_DIR;
    char path[MAX_FILENAME_LEN];
    int path_len = snprintf(path, sizeof(path) - 1, "%s/new", maildir_root);
    path[path_len] = '\0';

    DIR *d = opendir(path);
    if (!d) {
        fprintf(stderr, "Ошибка при попытке открыть дирректорию с письмами\n");
        exit(EXIT_FAILURE);
    }
    return d;
}

void get_new_mail_path(char *file, char *result)
{
    char *maildir_root = MAIL_DIR;
    char path_in_new_dir[MAX_FILENAME_LEN];
    int new_path_len = snprintf(path_in_new_dir, sizeof(path_in_new_dir) - 1, "%s/new/%s", maildir_root, file);
    path_in_new_dir[new_path_len] = '\0';


    uuid_t guid;
    uuid_generate_time(&guid);
    char guid_str[50];
    uuid_unparse(guid, guid_str);


    char path_in_tmp_dir[MAX_FILENAME_LEN];
    int tmp_path_len = snprintf(path_in_tmp_dir, sizeof(path_in_tmp_dir) - 1, "%s/tmp/%s-%s", maildir_root, file, guid_str);
    path_in_tmp_dir[tmp_path_len] = '\0';

    int ret = rename(path_in_new_dir, path_in_tmp_dir);
    if (ret != 0) {
        printf("Ошибка при перемещении файла %s в папку tmp\n", path_in_new_dir);
        strcpy(result, "");
    } else {
        strcpy(result, path_in_tmp_dir);
    }

}

