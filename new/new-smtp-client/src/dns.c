#include "dns.h"

#include <arpa/inet.h>
#include "firedns.h"
#include "string.h"

#include "config.h"

int resolve_mx_ip(struct Mail *mail, char *got_ip)
{
    struct firedns_mxlist *lst= firedns_resolvemxlist(mail->domain);
    if (lst == NULL) {
        return -1;
    }
    struct in_addr *result = firedns_resolveip4(lst->name);
    char *ip = inet_ntoa(*result);

// хак для локального тестирования
    if (strcmp(lst->name, "mxs.mail.ru") == 0) {
        strcpy(got_ip, "127.0.0.16");
    } else if (strcmp(lst->name, "mx.yandex.ru") == 0) {
        strcpy(got_ip, "127.0.0.18");
    } else {
        strcpy(got_ip, ip);

    }


    return 0;
}
