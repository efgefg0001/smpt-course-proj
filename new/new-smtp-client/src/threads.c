#include "threads.h"

#include "config.h"
#include "mail_q.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static void* zmq_ctx;

static pthread_t workers[100];

void set_zmq_ctx(void* ctx)
{
    zmq_ctx = ctx;
}


void *worker_func(void *args)
{
    int id = *((int*)args);
    fprintf(stderr, "id = %d\n", id);

    struct MailQ mail_q = new_MailQ(zmq_ctx, id, 0);

    while (1)
    {
        char path[MAIL_PATH_MAX_SIZE];
        fprintf(stderr, "before get from mailq\n");
        get_from_MailQ_blocking(&mail_q, path);
        process_got_mail(path);

        fprintf(stderr, "id = %d, path = %s\n", id, path);
    }
    return 0;
}

void start_workers()
{
    for (int i = 0; i < WORKERS_COUNT; ++i)
    {
        int *data = malloc(sizeof(int));
        *data = i;
        pthread_create(&(workers[i]), NULL, worker_func, data);
    }

}

void wait_workers()
{
   for (int i = 0; i < WORKERS_COUNT; ++i)
   {
       pthread_join(workers[i], NULL);
   }
}

int read_mail_from_maildir(char *filename, struct Mail *mails, int *emails_count)
{
    FILE *file = fopen(filename, "rt");
    if (!file) {
       fprintf(stderr, "Не удалось открыть файл '%s'\n", filename);
       return -1;
    }
    char mail_from[MAX_ADDR_LEN];
    fscanf(file, "%s", mail_from);
    if (!strstr(mail_from, "MAIL_FROM:")) {
       fprintf(stderr, "Не указан MAIL_FROM'%s'\n", filename);
       return -2;
    }
    fscanf(file, "%s", mail_from);
    char rcpt_to[MAX_MAIL_DATA_LEN];
    fscanf(file, "%s", rcpt_to);
    if (!strstr(rcpt_to, "RCPT_TO:")) {
       fprintf(stderr, "Не указан RCPT_TO'%s'\n", filename);
       return -3;
    }
    fgets(rcpt_to, sizeof(rcpt_to), file);
    char emails[20][MAX_ADDR_LEN];
    *emails_count = 0;
    char *pch = strtok (rcpt_to," \t\n");
    // считываю почтовый адреса
    while (pch != NULL)
    {
        strcpy(emails[*emails_count], pch);
        pch = strtok (NULL, " \t\n");
        ++(*emails_count);
    }

    char temp_var[MAX_MAIL_DATA_LEN];
    char mail_data[MAX_MAIL_DATA_LEN];

    strcpy(mail_data, "");


    while (fgets(temp_var, MAX_MAIL_DATA_LEN, file) != NULL)
        strcat(mail_data, temp_var);

    printf("mail_data = %s\n", mail_data);
    int mails_size = 0;
    for (int i = 0; i < *emails_count; ++i) {
       char domain[50];
       strcpy(domain, strstr(emails[i], "@") + 1);
        // убираем небуквы с конца строки
        for (int last = strlen(domain) -1; last >=0 && !isalpha(domain[last]); last--) {
            domain[last] = '\0';
        }

        int index = find_domain(domain, mails, mails_size);

        if (index != -1) {
            // если есть уже такое домен
            struct Mail mail = mails[index];
            strcpy(mail.email_addrs[mail.email_addrs_count], emails[i]);
            mail.email_addrs_count += 1;
            mails[index] = mail;
        } else {
            // если это новый домен
            struct Mail mail;
            strcpy(mail.domain, domain);
            strcpy(mail.from, mail_from);
            mail.email_addrs_count = 1;
            strcpy(mail.email_addrs[0], emails[i]);
            strcpy(mail.data, mail_data);
            mails[mails_size] = mail;
            mails_size += 1;
        }
    }
    *emails_count = mails_size;
    return 0;
}

int find_domain(char *domain, struct Mail *arr, int mail_count) {
    for (int i = 0; i < mail_count; ++i) {
        struct Mail cur = arr[i];
        if (strcmp(domain, cur.domain) == 0)	 {
            return i;
        }
    }
    return -1;
}
