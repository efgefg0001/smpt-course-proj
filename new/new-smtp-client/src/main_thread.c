#include "main_thread.h"

#include "mail_q.h"
#include "config.h"
#include "maildir.h"

#include <stdio.h>
#include <dirent.h>

void main_thread(void *ctx)
{
    struct MailQ mail_q[100];

    for (int i = 0; i < WORKERS_COUNT; ++i)
    {
        mail_q[i] = new_MailQ(ctx, i, 1);
    }
    int cur_worker = 0;
    while (1) {
        sleep(5);
        process_new_mails(mail_q, &cur_worker);
    }

}

void process_new_mails(struct MailQ mail_q[], int *cur_worker) {
    DIR *mail_dir =  open_new_dir();
    if (mail_dir) {
        struct dirent *file;
        while ((file = readdir(mail_dir)) != NULL)
        {
            /* пропуск всех нерегулярных файлов */
            if (file->d_type != DT_REG)
                continue;
            char *new_mail[MAX_FILENAME_LEN];
             get_new_mail_path(file->d_name, new_mail);
            if (strlen(new_mail) == 0)
                continue;
            add_to_MailQ(&mail_q[*cur_worker], new_mail);
            *cur_worker = (*cur_worker + 1) % WORKERS_COUNT;
        }
        closedir(mail_dir);
    }
}
