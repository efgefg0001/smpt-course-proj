#include <stdio.h>
#include <zmq.h>
#include <sys/socket.h>
#include <errno.h>

#include "threads.h"
#include "main_thread.h"
#include "string.h"

#include "remotes.h"

#include "firedns.h"

#include "log.h"

int main(int argc, char **argv) {

    firedns_init();

    log_init();

    void* ctx = zmq_ctx_new();

    set_zmq_ctx(ctx);

    start_workers();

    main_thread(ctx);

    wait_workers();


    return 0;
}
