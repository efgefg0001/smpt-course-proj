#include "mail_q.h"

#include <zmq.h>
#include <string.h>
#include <stdlib.h>

#define M_RECIEVE_BUFFER_SIZE 8192

void terminate(char *path)
{
    printf("Can't connect to socket '%s'\n", path);
    exit(1);
}

struct MailQ new_MailQ(void* zmq_context, int key, int is_writer)
{
    struct MailQ mq;
    mq.is_writer = is_writer;

    char path[MAIL_PATH_MAX_SIZE];
    sprintf(path, "ipc:///tmp/MailQ_%d", key);

    int rc = 0;

    if (!is_writer)
    {
        mq.zmq_socket = zmq_socket(zmq_context, ZMQ_SUB);
        rc = zmq_bind(mq.zmq_socket, path);
        zmq_setsockopt(mq.zmq_socket, ZMQ_SUBSCRIBE, "", 0);
    }
    else
    {
        mq.zmq_socket = zmq_socket(zmq_context, ZMQ_PUB);
        rc = zmq_connect(mq.zmq_socket, path);
    }

    if (rc != 0)
    {
        terminate(path);
    }

    return mq;
}

void close_MailQ(struct MailQ mail_q)
{
    zmq_close(mail_q.zmq_socket);
}

int add_to_MailQ(struct MailQ *mail_q, char *path)
{
    if (mail_q->is_writer ) {
        size_t size = strlen(path);
        int result = zmq_send(mail_q->zmq_socket, path, size, 0);

        if (result < 0)
        {
        return -1;
        }
        return 0;
    } else
        return -2;
}

int get_from_MailQ_blocking(struct MailQ *mail_q, char *mail_path)
{

    int size = zmq_recv(mail_q->zmq_socket, mail_path, MAIL_PATH_MAX_SIZE, 0);

    if (size < 0)
    {
        return -1;
    }

    return 0;
}
