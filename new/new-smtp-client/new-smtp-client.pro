QMAKE_CC = gcc
QMAKE_CXX = gcc
QMAKE_CXXFLAGS = -x c
QMAKE_CFLAGS = -Wall -std=gnu99
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lrt -lpthread -luuid -lzmq -luuid -lfiredns


SOURCES += \
    src/main.c \
    src/threads.c \
    src/main_thread.c \
    src/mail_q.c \
    src/maildir.c \
    src/remotes.c \
    src/dns.c \
    src/log.c

INCLUDEPATH += inc

#unix:!macx: LIBS += -L$$PWD/../../lib/ -lfiredns
#
#INCLUDEPATH += $$PWD/../../lib
#DEPENDPATH += $$PWD/../../lib
#
#unix:!macx: PRE_TARGETDEPS += $$PWD/../../lib/libfiredns.a

HEADERS += \
    ../../lib/firedns.h \
    inc/threads.h \
    inc/main_thread.h \
    inc/config.h \
    inc/mail_q.h \
    inc/maildir.h \
    inc/remotes.h \
    inc/dns.h \
    inc/log.h
